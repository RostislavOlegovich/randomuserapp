package com.slavamarinesko.randomuserapp.helper

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

@Suppress("unused")
inline fun <reified L : LiveData<T>, T> LifecycleOwner.observe(
    liveData: LiveData<T>,
    noinline block: (T) -> (Unit)
) {
    liveData.observe(this, Observer(block))
}
