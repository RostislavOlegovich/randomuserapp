package com.slavamarinesko.randomuserapp.helper

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.slavamarinesko.randomuserapp.R
import java.text.SimpleDateFormat
import java.util.*

@BindingAdapter("set_date")
fun setDate(view: TextView, date: String) {
    val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ROOT)
    val formatter = SimpleDateFormat("YYYY-mm-dd", Locale.ROOT)
    val output = formatter.format(parser.parse(date)!!)
    view.text = view.context.resources.getString(R.string.birthday, output)
}
