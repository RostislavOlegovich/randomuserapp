package com.slavamarinesko.randomuserapp.network

import retrofit2.Retrofit

class NetworkManagerImpl : NetworkManager {

    private val retrofit: Retrofit by lazy { NetworkInitializer.provideRetrofit() }
    private val api: Api by lazy { retrofit.create(Api::class.java) }

    override suspend fun loadUsers(page: Int) = api.loadUsers(page)
}