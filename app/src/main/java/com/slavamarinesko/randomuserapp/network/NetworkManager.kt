package com.slavamarinesko.randomuserapp.network

import com.slavamarinesko.randomuserapp.core.base.BaseManager
import com.slavamarinesko.randomuserapp.entity.Result

interface NetworkManager : BaseManager {

    suspend fun loadUsers(page: Int): Result
}