package com.slavamarinesko.randomuserapp.network

import com.slavamarinesko.randomuserapp.entity.Result
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("api/")
    suspend fun loadUsers(
        @Query(value = "page") page: Int,
        @Query(value = "results") results: Int = 20
    ): Result
}