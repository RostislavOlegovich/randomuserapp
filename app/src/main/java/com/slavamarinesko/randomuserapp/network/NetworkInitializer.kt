package com.slavamarinesko.randomuserapp.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object NetworkInitializer {

    private val okHttpClient: OkHttpClient = OkHttpClient()
    private val converterFactory: MoshiConverterFactory = MoshiConverterFactory.create()
    private val adapterFactory: CoroutineCallAdapterFactory = CoroutineCallAdapterFactory()

    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl("https://randomuser.me/")
        .client(okHttpClient)
        .addConverterFactory(converterFactory)
        .addCallAdapterFactory(adapterFactory)
        .build()
}