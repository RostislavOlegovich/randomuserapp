package com.slavamarinesko.randomuserapp.core.base

import android.util.Log
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import org.koin.core.KoinComponent

abstract class BaseViewModel : ViewModel(), KoinComponent, CoroutineScope {

    private val supervisorJob = SupervisorJob()

    override val coroutineContext = Dispatchers.Main.immediate + supervisorJob +
            CoroutineExceptionHandler { _, e -> onError(e) }

    override fun onCleared() {
        super.onCleared()
        coroutineContext.cancelChildren()
    }

    open fun onError(exception: Throwable) {
        Log.e("ERROR", "${exception.message}")
    }
}