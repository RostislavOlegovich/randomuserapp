package com.slavamarinesko.randomuserapp.core

import android.content.Context
import com.slavamarinesko.randomuserapp.feature.UsersViewModel
import com.slavamarinesko.randomuserapp.network.NetworkManager
import com.slavamarinesko.randomuserapp.network.NetworkManagerImpl
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

object AppInjector {

    fun setup(context: Context) {
        startKoin {
            androidContext(context)
            modules(listOf(viewModelsModule, managersModule))
        }
    }

    private val managersModule = module {
        single<NetworkManager> { NetworkManagerImpl() }
    }

    private val viewModelsModule = module {
        viewModel { UsersViewModel(get()) }
    }
}