package com.slavamarinesko.randomuserapp.core

import android.app.Application

@Suppress("unused")
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        AppInjector.setup(this)
    }
}