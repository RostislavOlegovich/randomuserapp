package com.slavamarinesko.randomuserapp.core.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.slavamarinesko.randomuserapp.entity.User

abstract class BaseAdapter<V : ViewDataBinding> :
    RecyclerView.Adapter<BaseAdapter.BaseViewHolder<V>>() {

    var onAction: ((Int) -> Unit)? = null

    open val items: MutableList<User> = mutableListOf()

    fun <V : ViewDataBinding> initBinding(
        @LayoutRes
        resId: Int,
        parent: ViewGroup,
        attachToParent: Boolean = false
    ): V =
        DataBindingUtil.inflate(LayoutInflater.from(parent.context), resId, parent, attachToParent)

    fun addItems(newItems: List<User>) {
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: BaseViewHolder<V>, position: Int) {
        if (isInPositionsRange(position)) {
            holder.itemView.setOnClickListener { onAction?.invoke(holder.adapterPosition) }
            holder.onBind(items[position])
        }
    }

    private fun isInPositionsRange(position: Int) =
        position != RecyclerView.NO_POSITION && position < itemCount

    abstract class BaseViewHolder<V : ViewDataBinding>(open val binding: V) :
        RecyclerView.ViewHolder(binding.root) {

        open fun onBind(item: User) {
            binding.setVariable(BR.item, item)
        }
    }
}