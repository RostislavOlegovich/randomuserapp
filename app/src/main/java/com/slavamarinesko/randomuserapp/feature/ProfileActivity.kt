package com.slavamarinesko.randomuserapp.feature

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.slavamarinesko.randomuserapp.R
import com.slavamarinesko.randomuserapp.core.base.BaseActivity
import com.slavamarinesko.randomuserapp.databinding.ActivityProfileBinding
import com.slavamarinesko.randomuserapp.entity.User


class ProfileActivity : BaseActivity<ActivityProfileBinding>() {

    override val assignLayoutId = R.layout.activity_profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val user = intent.getParcelableExtra<User>("user")
        binding.user = user
        loadImage()
        setupListener()
    }

    private fun loadImage() {
        Glide.with(this)
            .load(binding.user!!.picture.large)
            .apply(RequestOptions.circleCropTransform())
            .into(binding.photo)
    }

    private fun setupListener() {
        binding.phoneField.setOnClickListener {
            makePhoneCall()
        }
    }

    private fun makePhoneCall() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CALL)
        } else {
            startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:" + binding.user!!.cell)))
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        if (requestCode == REQUEST_CALL) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makePhoneCall()
            } else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object {
        private const val REQUEST_CALL = 1
    }
}
