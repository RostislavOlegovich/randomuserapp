package com.slavamarinesko.randomuserapp.feature

import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.slavamarinesko.randomuserapp.R
import com.slavamarinesko.randomuserapp.core.base.BaseAdapter
import com.slavamarinesko.randomuserapp.databinding.UserItemBinding


class UsersAdapter : BaseAdapter<UserItemBinding>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(initBinding(R.layout.user_item, parent))

    override fun onBindViewHolder(holder: BaseViewHolder<UserItemBinding>, position: Int) {
        super.onBindViewHolder(holder, position)
        Glide.with(holder.itemView.context)
            .load(items[position].picture.large)
            .apply(RequestOptions.circleCropTransform())
            .into(holder.binding.photo)
    }

    class ViewHolder(binding: UserItemBinding) : BaseViewHolder<UserItemBinding>(binding)
}