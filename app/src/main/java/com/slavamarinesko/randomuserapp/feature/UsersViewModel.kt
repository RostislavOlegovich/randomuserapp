package com.slavamarinesko.randomuserapp.feature

import androidx.lifecycle.MutableLiveData
import com.slavamarinesko.randomuserapp.core.base.BaseViewModel
import com.slavamarinesko.randomuserapp.entity.Result
import com.slavamarinesko.randomuserapp.network.NetworkManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UsersViewModel(private val networkManager: NetworkManager) : BaseViewModel() {

    val usersLD = MutableLiveData<Result>()

    fun loadUsers(page: Int) = launch {
        usersLD.value = withContext(Dispatchers.IO) {
            networkManager.loadUsers(page)
        }
    }
}