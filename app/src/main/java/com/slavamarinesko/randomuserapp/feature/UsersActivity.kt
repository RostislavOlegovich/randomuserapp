package com.slavamarinesko.randomuserapp.feature

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.slavamarinesko.randomuserapp.R
import com.slavamarinesko.randomuserapp.core.base.BaseActivity
import com.slavamarinesko.randomuserapp.databinding.ActivityUsersBinding
import com.slavamarinesko.randomuserapp.entity.Result
import com.slavamarinesko.randomuserapp.helper.PaginationScrollListener
import com.slavamarinesko.randomuserapp.helper.observe
import org.koin.android.viewmodel.ext.android.viewModel

class UsersActivity : BaseActivity<ActivityUsersBinding>() {

    override val assignLayoutId = R.layout.activity_users

    private lateinit var adapter: UsersAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager

    private val viewModel: UsersViewModel by viewModel()

    private var isLoading = false
    private var isLastPage = false
    private val startPage = 1
    private var currentPage = startPage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupUI()
        setupPaging()
        subscribeViewModel()
    }

    private fun loadPage(page: Int) = viewModel.loadUsers(page)

    private fun setupUI() {
        adapter = UsersAdapter()
        linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.recycler.apply {
            adapter = this@UsersActivity.adapter
            layoutManager = linearLayoutManager
        }
    }

    private fun subscribeViewModel() = observe(viewModel.usersLD) { setupData(it) }

    private fun setupPaging() {
        adapter.onAction = { position -> onClick(position) }
        binding.recycler.addOnScrollListener(object :
            PaginationScrollListener(linearLayoutManager) {

            override fun isLastPage() = isLastPage

            override fun isLoading() = isLoading

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                binding.progressPaging.visibility = View.VISIBLE
                loadPage(currentPage)
            }
        })
        loadPage(startPage)
    }

    private fun setupData(result: Result) {
        isLoading = false
        adapter.addItems(result.results)
        binding.progressBar.visibility = View.GONE
        binding.progressPaging.visibility = View.GONE
    }

    private fun onClick(position: Int) {
        val user = adapter.items[position]
        val intent = Intent(this, ProfileActivity::class.java)
        intent.putExtra("user", user)
        startActivity(intent)
    }
}
