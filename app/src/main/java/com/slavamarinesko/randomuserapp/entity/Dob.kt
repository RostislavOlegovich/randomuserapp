package com.slavamarinesko.randomuserapp.entity


import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Dob(
    @Json(name = "date")
    val date: String,
    @Json(name = "age")
    val age: Int
) : Parcelable