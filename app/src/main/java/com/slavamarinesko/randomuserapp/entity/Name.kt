package com.slavamarinesko.randomuserapp.entity


import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Name(
    @Json(name = "first")
    val first: String,
    @Json(name = "last")
    val last: String
) : Parcelable