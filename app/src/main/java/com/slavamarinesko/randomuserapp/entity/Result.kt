package com.slavamarinesko.randomuserapp.entity

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Result(
    @Json(name = "results")
    val results: @RawValue List<User>
) : Parcelable