package com.slavamarinesko.randomuserapp.entity


import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class User(
    @Json(name = "gender")
    val gender: String,
    @Json(name = "name")
    val name: @RawValue Name,
    @Json(name = "email")
    val email: String,
    @Json(name = "login")
    val login: @RawValue Login,
    @Json(name = "dob")
    val dob: @RawValue Dob,
    @Json(name = "phone")
    val phone: String,
    @Json(name = "cell")
    val cell: String,
    @Json(name = "picture")
    val picture: @RawValue Picture
) : Parcelable