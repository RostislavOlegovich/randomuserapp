package com.slavamarinesko.randomuserapp.entity


import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Login(
    @Json(name = "username")
    val username: String
) : Parcelable